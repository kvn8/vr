﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
   //Variables
   public float speed = 6.0F;
   public float jumpSpeed = 8.0F;
   public float gravity = 20.0F;
   private Vector3 moveDirection = Vector3.zero;

   void Update()
   {
      CharacterController controller = GetComponent<CharacterController>();
      // is the controller on the ground?
      if (controller.isGrounded)
      {
         if (Input.GetKey(KeyCode.LeftShift))
         {
            Debug.Log("ShiftDOWN");
            //Feed moveDirection with input.
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            //Multiply it by speed.
            moveDirection *= speed;

         }
         else
         {
            Debug.Log("Shift up");
            //Feed moveDirection with input.
            moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            //Multiply it by speed.
            moveDirection *= speed;
            var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
            transform.Rotate(0, x, 0);
         }
         //Jumping
         if (Input.GetButton("Jump"))
            moveDirection.y = jumpSpeed;

      }
      //Applying gravity to the controller
      moveDirection.y -= gravity * Time.deltaTime;
      //Making the character move
      controller.Move(moveDirection * Time.deltaTime);
   }

}
