﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Evernote.EDAM.Type;

public class RenderNotebooks : MonoBehaviour {

	public GameObject prefabNotebook;

	// Use this for initialization
	// change this to occur on a trigger

	void Awake () {
		Notebook[] nbs = { new Notebook() };
		nbs [0].Name = "Leo Gong";
			
		foreach (Notebook n in nbs) {
			ObjectFactory.CreateNotebook (n);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
