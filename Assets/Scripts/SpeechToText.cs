﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class SpeechToText : MonoBehaviour {

	private DictationRecognizer dr;
	private Microphone mic;

	// Use this for initialization
	void Start () {
		dr = new DictationRecognizer ();
		dr.DictationHypothesis += DictationHypothesisCallback;
		dr.Start ();
	}

	void DictationHypothesisCallback(string text) {
		Debug.Log (text);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
