﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Evernote.EDAM.Type;

public class ObjectFactory : MonoBehaviour {

	protected static ObjectFactory instance; // Needed for Singleton

	public GameObject notebookPrefab;

	void Start () {
		instance = this;
	}

	public static VRNotebook CreateNotebook(Notebook thriftNotebook) {
		VRNotebook notebook = Instantiate (instance.notebookPrefab,
			                      Vector3.zero, Quaternion.identity).GetComponent<VRNotebook> ();
		notebook.Initialize (thriftNotebook);
		return notebook;
	}

}
