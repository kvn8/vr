﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Evernote.EDAM.NoteStore;
using Evernote.EDAM.Type;
using Evernote.EDAM.UserStore;
using Thrift.Protocol;
using Thrift.Transport;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class MonolithScript : MonoBehaviour
{
   private NoteStore.Client _noteStore;
   private UserStore.Client _userStore;
   private bool _isConnected;
   private string _authToken;
   private NotesMetadataList _notes;
   private Note _selectedNote;

   public Transform NotebookPrefab;
   public Transform NoteSummaryPrefab;
   public Transform NoteFullPrefab;
   private List<Notebook> _notebooks;

   // Use this for initialization
   void Start()
   {
      System.Net.ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
      _camera = GameObject.FindObjectOfType<Camera>();
      GetNotebooks();
   }

   public void Connect()
   {
      // Real applications authenticate with Evernote using OAuth, but for the
      // purpose of exploring the API, you can get a developer token that allows
      // you to access your own Evernote account. To get a developer token, visit 
      // https://sandbox.evernote.com/api/DeveloperToken.action
      _authToken = "S=s1:U=93436:E=160defb7b0b:C=159874a4e58:P=1cd:A=en-devtoken:V=2:H=f1cd60bb4a9d4bc5420999329bf36d7e";

      // Initial development is performed on our sandbox server. To use the production 
      // service, change "sandbox.evernote.com" to "www.evernote.com" and replace your
      // developer token above with a token from 
      // https://www.evernote.com/api/DeveloperToken.action
      String evernoteHost = "sandbox.evernote.com";

      Uri userStoreUrl = new Uri("https://" + evernoteHost + "/edam/user");
      TTransport userStoreTransport = new THttpClient(userStoreUrl);
      TProtocol userStoreProtocol = new TBinaryProtocol(userStoreTransport);
      _userStore = new UserStore.Client(userStoreProtocol);

      bool versionOK =
          _userStore.checkVersion("Evernote EDAMTest (C#)",
            Evernote.EDAM.UserStore.Constants.EDAM_VERSION_MAJOR,
            Evernote.EDAM.UserStore.Constants.EDAM_VERSION_MINOR);
      Debug.Log("Is my Evernote API version up to date? " + versionOK);
      if (!versionOK)
      {
         return;
      }

      // Get the URL used to interact with the contents of the user's account
      // When your application authenticates using OAuth, the NoteStore URL will
      // be returned along with the auth token in the final OAuth request.
      // In that case, you don't need to make this call.
      String noteStoreUrl = _userStore.getNoteStoreUrl(_authToken);

      TTransport noteStoreTransport = new THttpClient(new Uri(noteStoreUrl));
      TProtocol noteStoreProtocol = new TBinaryProtocol(noteStoreTransport);
      _noteStore = new NoteStore.Client(noteStoreProtocol);
      _isConnected = true;
   }

   private List<Transform> _notebookObjects = new List<Transform>();
   private List<Transform> _noteSummaryObjects = new List<Transform>();
   private List<Transform> _noteFullObjects = new List<Transform>();
   private Camera _camera;
   private Transform _newNoteDetail;

   public void RemoveObjects()
   {
      RemoveNotebookObjects();
      RemoveNotesummaryObjects();
      RemoveNoteFullObjects();
   }
   void RemoveNotebookObjects()
   {
      _notebookObjects.ForEach(o => DestroyObject(o.gameObject));
      _notebookObjects.Clear();
   }
   void RemoveNotesummaryObjects()
   {
      _noteSummaryObjects.ForEach(o => DestroyObject(o.gameObject));
      _noteSummaryObjects.Clear();
   }
   void RemoveNoteFullObjects()
   {
      _noteFullObjects.ForEach(o => DestroyObject(o.gameObject));
      _noteFullObjects.Clear();
   }

   public void GetNotebooks()
   {
      if (!_isConnected) Connect();
      RemoveObjects();
      // List all of the notebooks in the user's account        
      _notebooks = _noteStore.listNotebooks(_authToken);
      Debug.Log("Found " + _notebooks.Count + " notebooks:");
		var z = 0;

      foreach (Notebook notebook in _notebooks)
      {
			z += 5;
			var newBook = Instantiate(NotebookPrefab, new Vector3(1, 1, z), Quaternion.identity);

			TextMesh textMesh = newBook.GetComponentInChildren<TextMesh> ();
			textMesh.text = notebook.Name;
			MegaBookBuilder builder = newBook.GetComponentInChildren<MegaBookBuilder> ();
			builder.NumPages = 2;
			builder.NextPage ();
			builder.NextPage ();
			builder.BuildPages ();



			var page1 = newBook.Find ("_Pages (1)").Find("Page1");
			page1.GetComponentInChildren<TypogenicText> ().Text = "Kchan wuz here";

			
			GetPages (notebook, newBook, z);

         _notebookObjects.Add(newBook);
         Debug.Log("  * " + notebook.Name);
      }
   }

	public static string StripHTML(string HTMLText)
	{
		Regex reg = new Regex("<[^>]+>", RegexOptions.IgnoreCase);
		var stripped = reg.Replace(HTMLText, "");
		return stripped;
	}
	public void GetPages(Notebook selectedNotebook, Transform newBook, int z) {
		var filter = new NoteFilter() { NotebookGuid = selectedNotebook.Guid };
		var resultSpec = new NotesMetadataResultSpec() { IncludeTitle = true };
		_notes = _noteStore.findNotesMetadata(_authToken, filter, 0, 100, resultSpec);

		var pages = newBook.Find ("_Pages (1)");
		MBComplexPage mbComplexPage = newBook.GetComponentInChildren<MBComplexPage> ();


		List<GameObject> _pages = new List<GameObject> ();

		for (var index = 0; index < _notes.Notes.Count (); index++) {
			var note = _notes.Notes [index];
			Note _selectedNote = _noteStore.getNote(_authToken, note.Guid, true, false, false, false);

			var newNote = Instantiate (NoteFullPrefab, new Vector3 (0, index, z), Quaternion.identity, pages.transform);
			newNote.GetComponentInChildren<TypogenicText> ().Text = StripHTML(_selectedNote.Content);

			_pages.Add(newNote.gameObject);
		}
		mbComplexPage.pages = _pages;

	}

   public void GetNotes(Notebook selectedNotebook)
   {
      int rows = 3;
      int columns = 4;
      int column = 0;

      float width = 3.7f;
      float height = 2.4f;
      float left = 569.3f;
      float top = 28.5f;

      if (selectedNotebook != null)
      {
         RemoveNotesummaryObjects();
         RemoveNoteFullObjects();

         var filter = new NoteFilter() { NotebookGuid = selectedNotebook.Guid };
         var resultSpec = new NotesMetadataResultSpec() { IncludeTitle = true };
         _notes = _noteStore.findNotesMetadata(_authToken, filter, 0, 100, resultSpec);
         for (var index = 0; (index < (rows*columns) && index < _notes.Notes.Count()) ; index++)
         {
            var note = _notes.Notes[index];
            column = index % columns;
            var useLeft = left + (column * width);
            int row = index / columns;
            var useTop = top - (row * height);
            var newSummary = Instantiate(NoteSummaryPrefab, new Vector3(useLeft, useTop, 0f), Quaternion.identity);

            var canvas = newSummary.GetComponentInChildren<Canvas>();
            canvas.worldCamera = _camera;

            var texts = newSummary.GetComponentsInChildren<Text>();
            texts[0].text = note.Title;
            var button = newSummary.GetComponentInChildren<Button>();
            var note1 = note;
            button.onClick.AddListener(() => OpenNote(note1));
            //texts[1].text = note;
            _noteSummaryObjects.Add(newSummary);
         }
      }
   }

   private void OpenNote(NoteMetadata note)
   {
      _selectedNote = _noteStore.getNote(_authToken, note.Guid, true, false, false, false);
      _newNoteDetail = Instantiate(NoteFullPrefab, new Vector3(570.3f, 24.5f, -3.5f), Quaternion.identity);

      var canvas = _newNoteDetail.GetComponentInChildren<Canvas>();
      canvas.worldCamera = _camera;

      var texts = _newNoteDetail.GetComponentsInChildren<Text>();
      texts[0].text = _selectedNote.Title;
      texts[1].text = _selectedNote.Content;
      texts[2].text = _selectedNote.Created.ToString();
      var button = _newNoteDetail.GetComponentInChildren<Button>();
      button.onClick.AddListener(() => CloseNoteDetail());
   }

   private void CloseNoteDetail()
   {
      if (_selectedNote != null)
      {
         Destroy(_newNoteDetail.gameObject);
         _newNoteDetail = null;
         _selectedNote = null;
      }
   }

   public void GetNote(Guid noteGuid)
   {
      _selectedNote = _noteStore.getNote(_authToken, noteGuid.ToString(), false, false, false, false);
   }

   // Update is called once per frame
   void Update()
   {

   }
}
